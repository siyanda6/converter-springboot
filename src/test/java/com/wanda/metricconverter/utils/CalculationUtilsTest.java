package com.wanda.metricconverter.utils;

import com.wanda.metricconverter.pojos.Output;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CalculationUtilsTest {

    @Autowired
    CalculationUtils calculationUtils;

    @Test
    public void tempCelciusToFahrenheitTest() {

        Output out = calculationUtils.calculateTemp(23.0, "C");
        assert(out != null);

        System.out.println("Temperature in degrees Fahrenheit is = " + out.getValue());
    }

    @Test
    public void tempFahrenheitToCelciusTest() {

        Output out = calculationUtils.calculateTemp(100.0, "F");
        assert(out != null);

        System.out.println("Temperature in degrees Celcius is = " + out.getValue());
    }

    @Test
    public void lengthKmToMileTest() {

        Output out = calculationUtils.calculateLength(1.0, "km");
        assert(out != null);

        System.out.println("Length in Miles is = " + out.getValue());
    }

    @Test
    public void lengthMileToKmTest() {

        Output out = calculationUtils.calculateLength(3.0, "mi");
        assert(out != null);

        System.out.println("Length in kilometers is = " + out.getValue());
    }

    @Test
    public void massKgToPoundTest() {

        Output out = calculationUtils.calculateMass(7, "kg");
        assert(out != null);

        System.out.println("Mass in kilograms is = " + out.getValue());
    }

    @Test
    public void massPoundToKgTest() {

        Output out = calculationUtils.calculateMass(50.0, "lb");
        assert(out != null);

        System.out.println("Mass in pounds is = " + out.getValue());
    }

    @Test
    public void volumeLitresToGallonsTest() {

        Output out = calculationUtils.calculateVolume(13.0, "l");
        assert(out != null);

        System.out.println("Volume in litres is = " + out.getValue());
    }

    @Test
    public void volumeGallonsToLitresTest() {

        Output out = calculationUtils.calculateVolume(85.0, "gal");
        assert(out != null);

        System.out.println("Volume in gallons is = " + out.getValue());
    }

    @Test
    public void areaSquareMToSquareFtTest() {

        Output out = calculationUtils.calculateArea(2.0, "m^2");
        assert(out != null);

        System.out.println("Area in square meter is = " + out.getValue());
    }

    @Test
    public void areaSquareFtToSquareMTest() {

        Output out = calculationUtils.calculateArea(100.0, "F");
        assert(out != null);

        System.out.println("Area in square feet is = " + out.getValue());
    }
}
