package com.wanda.metricconverter.services;

import com.wanda.metricconverter.pojos.Input;
import com.wanda.metricconverter.pojos.Metric;
import com.wanda.metricconverter.pojos.Output;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ServicesTest {

    @Autowired
    ConverterService converterService;

    @Test
    public void tempCelciusToFahrenheitTest() {

        try{
            Output out = converterService.convert(23.00, "C", "temp");
            assert(out != null);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Test
    public void getAllMetricsTest() {

        try{
            List<Metric> metrics = converterService.getAllMetrics();
            assert(metrics.size() > 0);

            metrics.stream().forEach(s -> System.out.println("Metric: " + s.getMetric() +
                                                                ", Metric Unit: " + s.getMetricUnit() +
                                                                ", Imperial Unit: " + s.getImperialUnit()));
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
