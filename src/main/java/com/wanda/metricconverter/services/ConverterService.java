package com.wanda.metricconverter.services;

import com.wanda.metricconverter.enums.ConverterEnum;
import com.wanda.metricconverter.pojos.Metric;
import com.wanda.metricconverter.pojos.Output;
import com.wanda.metricconverter.utils.CalculationUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ConverterService {

    @Autowired
    CalculationUtils calculationUtils;

    public List<Metric> getAllMetrics() {

        List<Metric> metrics = new ArrayList<>();

        for(ConverterEnum converterEnum : ConverterEnum.values()) {

            Metric metric = new Metric();
            metric.setMetric(converterEnum.getMetric());
            metric.setImperialUnit(converterEnum.getImperialUnit());
            metric.setMetricUnit(converterEnum.getMetricUnit());

            metrics.add(metric);
        }

        return metrics;
    }

    public Output convert(double value, String unit, String metric)  throws Exception{

        if(StringUtils.equals(metric, ConverterEnum.TEMP.getMetric())) {

            return calculationUtils.calculateTemp(value, unit);

        }else if(StringUtils.equals(metric, ConverterEnum.LENGTH.getMetric())) {

            return calculationUtils.calculateLength(value, unit);

        }else if(StringUtils.equals(metric, ConverterEnum.MASS.getMetric())) {

            return calculationUtils.calculateMass(value, unit);

        }else if(StringUtils.equals(metric, ConverterEnum.VOLUME.getMetric())) {

            return calculationUtils.calculateVolume(value, unit);

        }else if(StringUtils.equals(metric, ConverterEnum.AREA.getMetric())) {

            return calculationUtils.calculateArea(value, unit);

        }else{
            throw new Exception();
        }
    }
}
