package com.wanda.metricconverter.controllers;

import com.wanda.metricconverter.pojos.Metric;
import com.wanda.metricconverter.pojos.Output;
import com.wanda.metricconverter.services.ConverterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "converter", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
public class ConverterController {

    @Autowired
    ConverterService converterService;

    @CrossOrigin
    @GetMapping("/get-all-metrics")
    public ResponseEntity<List<Metric>> getAllMetrics() {

        try {

            System.out.println("now In");

            List<Metric> metrics = converterService.getAllMetrics();

            return ResponseEntity.ok(metrics);

        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @CrossOrigin
    @GetMapping("/convert/{value}/{unit}/{metric}")
    public ResponseEntity<Output> convert(@PathVariable double value,
                                          @PathVariable String unit,
                                          @PathVariable String metric) {

        try {

            System.out.println("Inside");

            Output output = converterService.convert(value, unit, metric);

            return ResponseEntity.ok(output);

        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
