package com.wanda.metricconverter.utils;

import com.wanda.metricconverter.enums.ConverterEnum;
import com.wanda.metricconverter.pojos.Output;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class CalculationUtils {

    public Output calculateTemp(double input, String inputUnit) {

        double result;
        Output output = new Output();

        if(StringUtils.equals(inputUnit, ConverterEnum.TEMP.getMetricUnit())) {
            result = (input * 9/5) + 32;
            output.setUnit(ConverterEnum.TEMP.getImperialUnit());
        }else {
            result = (input - 32) * 5/9;
            output.setUnit(ConverterEnum.TEMP.getMetricUnit());
        }

        output.setValue(result);
        return output;
    }

    public Output calculateLength(double input, String inputUnit) {

        double result;
        Output output = new Output();

        if(StringUtils.equals(inputUnit, ConverterEnum.LENGTH.getMetricUnit())) {
            result = input/1.609;
            output.setUnit(ConverterEnum.LENGTH.getImperialUnit());
        }else {
            result = input * 1.609;
            output.setUnit(ConverterEnum.LENGTH.getMetricUnit());
        }

        output.setValue(result);
        return output;
    }

    public Output calculateMass(double input, String inputUnit) {

        double result;
        Output output = new Output();

        if(StringUtils.equals(inputUnit, ConverterEnum.MASS.getMetricUnit())) {
            result = input * 2.205;
            output.setUnit(ConverterEnum.MASS.getImperialUnit());
        }else {
            result = input/2.205;
            output.setUnit(ConverterEnum.MASS.getMetricUnit());
        }

        output.setValue(result);
        return output;
    }

    public Output calculateVolume(double input, String inputUnit) {

        double result;
        Output output = new Output();

        if(StringUtils.equals(inputUnit, ConverterEnum.VOLUME.getMetricUnit())) {
            result = input/4.546;
            output.setUnit(ConverterEnum.VOLUME.getImperialUnit());
        }else {
            result = input * 4.546;
            output.setUnit(ConverterEnum.VOLUME.getMetricUnit());
        }

        output.setValue(result);
        return output;
    }

    public Output calculateArea(double input, String inputUnit) {

        double result;
        Output output = new Output();

        if(StringUtils.equals(inputUnit, ConverterEnum.AREA.getMetricUnit())) {
            result = input * 10.764;
            output.setUnit(ConverterEnum.AREA.getImperialUnit());
        }else {
            result = input/10.764;
            output.setUnit(ConverterEnum.AREA.getMetricUnit());
        }

        output.setValue(result);
        return output;
    }
}
