package com.wanda.metricconverter.enums;

public enum ConverterEnum {

    TEMP("temp", "C", "F"),
    LENGTH("length", "km", "mi"),
    MASS("mass", "kg", "lb"),
    VOLUME("volume", "l", "gal"),
    AREA("area", "m^2", "ft^2");

    private String metric;
    private String metricUnit;
    private String imperialUnit;

    ConverterEnum(String metric, String metricUnit, String imperialUnit) {
        this.metric = metric;
        this.metricUnit = metricUnit;
        this.imperialUnit = imperialUnit;

    }

    public String getMetric() {
        return metric;
    }

    public String getMetricUnit() {
        return metricUnit;
    }

    public String getImperialUnit() {
        return imperialUnit;
    }
}
