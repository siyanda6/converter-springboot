package com.wanda.metricconverter.constants;

public interface ConverterConstants {

    String TEMP = "temp";
    String CELCIUS = "C";
    String FAHRENHEIT = "F";

    String LENGTH = "length";
    String KM = "km";
    String MILE = "mi";

    String MASS = "mass";
    String KG = "kg";
    String POUND = "lb";

    String VOLUME = "volume";
    String CUBIC_METER = "m^3";
    String CUBIC_FEET = "ft^3";

    String AREA = "area";
    String SQUARE_METER = "m^2";
    String SQUARE_FEET = "ft^2";

}
