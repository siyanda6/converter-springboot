package com.wanda.metricconverter.pojos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Input {

    private double value;
    private double unit;
    private String metric;
}
