package com.wanda.metricconverter.pojos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Metric {

    private String metric;
    private String metricUnit;
    private String imperialUnit;
}
