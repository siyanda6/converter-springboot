package com.wanda.metricconverter.pojos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Output {

    private double value;
    private String unit;
}
